Ciao a tutti, il mio nome è Vincenzo Fiore e sono un programmatore.
Questi sono i miei file, che ho deciso di rendere pubblici, in quanto relativi agli alias che ho deciso di utilizzare per git e per i comandi in console.

Per coloro che non fossero a conoscenza di cosa sono gli alias, gli alias sono delle abbreviazioni di comandi, che vanno a sostituire il comando originario, rendendo agevole
la programmazione:

esempio: sono in console e so che per fare il chackout su git devo digitare il seguente comando:
git checkout ....

..benissimo, grazie agli alias posso facilitarmi il lavoro, lanciando lo stesso comando, ma in maniera abbreviata:

git checkout = git co